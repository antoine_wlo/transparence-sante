CREATE SCHEMA IF NOT EXISTS monitor;
SET search_path TO monitor, public;

DROP EXTENSION IF EXISTS pg_cron;
CREATE EXTENSION IF NOT EXISTS pg_cron;
CREATE EXTENSION IF NOT EXISTS file_fdw;
CREATE EXTENSION IF NOT EXISTS pg_stat_statements;

DROP SERVER IF EXISTS fileserver CASCADE ;
CREATE SERVER fileserver FOREIGN DATA WRAPPER file_fdw;

-- MONITOR SYSTEM CPU & MEMORY
-- using a mapping of filesystem /proc/loadavg & /proc/meminfo
-- https://aaronparecki.com/2015/02/19/8/monitoring-cpu-memory-usage-from-postgres

-- view on system infos
CREATE FOREIGN TABLE IF NOT EXISTS loadavg
  (one text, five text, fifteen text, scheduled text, pid text)
  SERVER fileserver
  OPTIONS (filename '/proc/loadavg', format 'text', delimiter ' ');

CREATE FOREIGN TABLE IF NOT EXISTS meminfo
  (stat text, value text)
  SERVER fileserver
  OPTIONS (filename '/proc/meminfo', format 'csv', delimiter ':');

-- create a table to store system metrics, then cron insert
CREATE TABLE IF NOT EXISTS metrics AS
  SELECT
    now(),
    substring(meminfo.value, ' *(\d*) kB') :: int as available_memory_kb,
    loadavg.one :: float                          as load_average_one
  FROM meminfo, loadavg
  WHERE meminfo.stat = 'MemAvailable';

SELECT cron.schedule('0 * * * *',
                     $$
INSERT INTO monitor.metrics
  SELECT
    now(),
    substring(meminfo.value, ' *(\d*) kB') :: int as available_memory_kb,
    loadavg.one :: float as load_average_one
  FROM monitor.meminfo, monitor.loadavg
  WHERE meminfo.stat = 'MemAvailable';
                     $$);



-- MONITOR POSTGRES USAGE BY METABASE'S USERS
-- using pg_stat_statements
-- Example of a query issued by Metabase
/*
Metabase:: userID: 1 queryType: MBQL queryHash: 96297fb5aabe8bf3b5b104b3752f4cfff759c03e2d952b47ad83c6d4fa81f5aa
SELECT count(*) AS "count"
FROM "public"."declaration"
*/

-- view on pg_stat_statements by metabases' users
CREATE OR REPLACE VIEW pg_stat_statements_metabase AS
  WITH tmp AS (
      SELECT
        substring(query, '-- Metabase:: userID: (\d+)') :: int   as metabase_user_id,
        substring(query, 'queryType: MBQL queryHash: .*\n(.*)$') as sql_query,
        calls,
        total_time,
        min_time,
        max_time,
        mean_time,
        rows,
        shared_blks_hit,
        shared_blks_read

      FROM pg_stat_statements
  )
  SELECT *
  FROM tmp
  WHERE metabase_user_id IS NOT NULL;


-- create a table to store postgres' usage by user, then cron insert
CREATE TABLE IF NOT EXISTS postgres_usage_by_user AS
  SELECT
    now(),
    sum(total_time) as total_user_time,
    metabase_user_id
  FROM monitor.pg_stat_statements_metabase
  GROUP BY metabase_user_id;


SELECT cron.schedule('0 * * * *',
                     $$
                     INSERT INTO monitor.postgres_usage_by_user
                       SELECT
                         now(),
                         sum(total_time) as total_user_time,
                         metabase_user_id
                       FROM monitor.pg_stat_statements_metabase
                       GROUP BY metabase_user_id;
                     $$);

-- create a view to store additional use during last hour
CREATE OR REPLACE VIEW monitor.hour_delta_by_user AS
  WITH
      active_user AS (
        SELECT metabase_user_id
        FROM monitor.pg_stat_statements_metabase
        GROUP BY metabase_user_id
    ),
      previous_hour_total_by_user AS (
        SELECT
          max(total_user_time) as total_user_time,
          metabase_user_id
        FROM monitor.postgres_usage_by_user
        WHERE now() - now > interval '1 hour'
        GROUP BY metabase_user_id
    ),
      previous_hour_total_by_user_with_default AS (
        SELECT
          COALESCE(total_user_time, 0) as total_user_time,
          active_user.metabase_user_id
        FROM active_user
          LEFT JOIN previous_hour_total_by_user
            ON active_user.metabase_user_id = previous_hour_total_by_user.metabase_user_id
    ),
      current_total_by_user AS (
        SELECT
          sum(total_time) as total_user_time,
          metabase_user_id
        FROM monitor.pg_stat_statements_metabase
        GROUP BY metabase_user_id
    ),
      delta_by_user_since_previous_hour AS (
        SELECT
          current.total_user_time - previous.total_user_time as delta_time_by_user,
          current.metabase_user_id
        FROM current_total_by_user as current
          JOIN previous_hour_total_by_user_with_default as previous
            ON current.metabase_user_id = previous.metabase_user_id
    )
  SELECT
    metabase_user_id,
    delta_time_by_user
  FROM delta_by_user_since_previous_hour
  WHERE delta_time_by_user > 0;
