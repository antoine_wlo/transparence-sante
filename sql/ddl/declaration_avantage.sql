
CREATE TABLE "declaration_avantage" (
    "id" SERIAL PRIMARY KEY,
    "catégorie_beneficiaire" TEXT,
    "date" DATE,
    "declaration_id" TEXT,
    "detail" TEXT,
    "entreprise_émmetrice" TEXT,
    "filiale_déclarante" TEXT,
    "identifiant" TEXT,
    "identifiant_convention" TEXT,
    "identifiant_entreprise" TEXT,
    "montant_ttc" NUMERIC,
    "nom_prénom" TEXT,
    "profession" TEXT,
    "specialité" TEXT,
    "structure_bénéficiaire" TEXT,
    "type_declaration" TEXT,
    "type_identifiant" TEXT
);
