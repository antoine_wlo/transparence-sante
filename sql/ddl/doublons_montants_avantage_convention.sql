
SET work_mem = '150MB'; -- allow HashAggregate rather than a slow GroupAggregate
CREATE MATERIALIZED VIEW "doublons_montants_avantage_convention" AS (
    WITH a AS (
        SELECT
             identifiant_entreprise,
             COALESCE(nom_prénom, 'Ø') as nom_prénom,
             COALESCE(structure_bénéficiaire, 'Ø') as structure_bénéficiaire,
             CAST(date AS date),
             type_declaration,
             sum(montant_ttc) as montant_ttc
        FROM declaration
        WHERE
            type_declaration != 'Remuneration'
        GROUP BY
             identifiant_entreprise,
             nom_prénom,
             structure_bénéficiaire,
             CAST(date AS date),
             type_declaration
    ),
    advantage AS (SELECT * FROM a WHERE a.type_declaration = 'Avantage'),
    convention AS (SELECT * FROM a WHERE a.type_declaration = 'Convention'),
    doublons AS (
        SELECT *
        FROM advantage INNER JOIN convention
          USING (nom_prénom, structure_bénéficiaire, date, identifiant_entreprise, montant_ttc)
    )
    SELECT
       company.filiale_déclarante,
       company.entreprise_émmetrice,
       doublons.identifiant_entreprise,
       doublons.nom_prénom,
       doublons.structure_bénéficiaire,
       doublons.date,
       doublons.montant_ttc
    FROM doublons LEFT JOIN entreprise as company using (identifiant_entreprise)

);
RESET work_mem;

CREATE INDEX IF NOT EXISTS "doublons_montants_avantage_convention_prefix_index_on_entreprise_émmetrice"
ON public."doublons_montants_avantage_convention" (lower("entreprise_émmetrice") text_pattern_ops, "entreprise_émmetrice");

CREATE INDEX IF NOT EXISTS "doublons_montants_avantage_convention_index_on_entreprise_émmetrice"
ON public."doublons_montants_avantage_convention" ("entreprise_émmetrice");

CREATE INDEX IF NOT EXISTS "doublons_montants_avantage_convention_prefix_index_on_nom_prénom"
ON public."doublons_montants_avantage_convention" (lower("nom_prénom") text_pattern_ops, "nom_prénom");

CREATE INDEX IF NOT EXISTS "doublons_montants_avantage_convention_index_on_nom_prénom"
ON public."doublons_montants_avantage_convention" ("nom_prénom");

CREATE INDEX IF NOT EXISTS "doublons_montants_avantage_convention_prefix_index_on_structure_bénéficiaire"
ON public."doublons_montants_avantage_convention" (lower("structure_bénéficiaire") text_pattern_ops, "structure_bénéficiaire");

CREATE INDEX IF NOT EXISTS "doublons_montants_avantage_convention_index_on_structure_bénéficiaire"
ON public."doublons_montants_avantage_convention" ("structure_bénéficiaire");
