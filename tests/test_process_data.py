import os

import pytest

from main import all_cleaners
from main import all_views_ddl, all_uploaders
from src.constants.csv import DEFAULT_SEPARATOR
from src.constants.directories import EXPECTED
from src.constants.directories import TEST, SQL
from src.specific.declaration import advantage, convention, remuneration
from src.utils.utils import add_final_directory_to_file_path


def test_that_all_declaration_have_the_same_columns_list():
    advantage_columns = set(advantage.cleaner._cleaned_csv_columns)
    remuneration_columns = set(remuneration.cleaner._cleaned_csv_columns)
    convention_columns = set(convention.cleaner._cleaned_csv_columns)
    assert advantage_columns == remuneration_columns
    assert advantage_columns == convention_columns


@pytest.mark.parametrize("cleaner", all_cleaners())
def test_cleaned_equals_expected(cleaner):
    # Given
    expected_csv_path = add_final_directory_to_file_path(cleaner.output_csv_path, EXPECTED)
    with open(expected_csv_path, encoding='utf-8', mode='r') as expected_csv:
        expected_csv_content = expected_csv.read()

    # When
    cleaner.run()
    with open(cleaner.output_csv_path, encoding='utf-8', mode='r') as produced_csv:
        produced_csv_content = produced_csv.read()

    # Then
    expected_csv_lines = expected_csv_content.split('\n')
    produced_csv_lines = produced_csv_content.split('\n')

    for expected_column, produced_column in zip(expected_csv_lines[0].split(DEFAULT_SEPARATOR),
                                                produced_csv_lines[0].split(DEFAULT_SEPARATOR)):
        assert expected_column == produced_column

    for line_expected, line_actual in zip(expected_csv_lines, produced_csv_lines):
        assert line_expected == line_actual
    assert produced_csv_content == expected_csv_content


# Avoid metabase errors in case of case change  # https://github.com/metabase/metabase/issues/7923
@pytest.mark.parametrize("cleaner", all_cleaners())
def test_that_cleaned_columns_are_lower_case(cleaner):
    for column_name in cleaner._cleaned_csv_columns:
        assert column_name.islower()


def test_uploader_ddl_in_sync_with_file():
    ddl_list = [uploader.table_ddl for uploader in all_uploaders()]
    ddl_list += all_views_ddl()
    for ddl in ddl_list:
        # Write ddl to file to evaluate differences
        test_ddl_path = os.path.join(TEST, SQL, ddl.name + '.sql')
        with open(test_ddl_path, 'wb') as ddl_file:
            ddl_file.write(ddl.query.encode("utf-8"))

        print(ddl.name)
        with open(ddl.path, encoding='utf-8', mode='r') as ddl_file:
            ddl_file_content = ddl_file.read()

        # Then
        for line_expected, line_actual in zip(ddl.query.split('\n'), ddl_file_content.split('\n')):
            assert line_expected == line_actual
        assert ddl.query == ddl_file_content
