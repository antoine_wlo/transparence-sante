import os

from dotenv import load_dotenv

from src.constants.connexions import LOCALHOST

load_dotenv()

ROWS_LIMIT = os.getenv('ROWS_LIMIT')
if ROWS_LIMIT is not None:
    ROWS_LIMIT = int(ROWS_LIMIT)

POSTGRES_HOST = os.getenv('POSTGRES_HOST', LOCALHOST)
POSTGRES_PASSWORD = os.getenv('POSTGRES_PASSWORD', '')
POSTGRES_PORT = int(os.getenv('POSTGRES_PORT', '5432'))

# Memory related
POSTGRES_WORK_MEM = os.getenv('POSTGRES_WORK_MEM', '150MB')
CSV_CHUNK_SIZE = os.getenv('CSV_CHUNK_SIZE', 10**5)

# Only for tests. Defined in pytest.ini
DATA_ROOT_DIR = os.getenv('DATA_ROOT_DIR', '')

