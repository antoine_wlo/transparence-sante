import gzip
import shutil
from os.path import join as p_join

from src.constants.directories import DUMP_DATA_DIR
from src.utils.utils import timeit, human_size


class Dumper:
    def __init__(self, directory, file_name):
        self.directory = directory
        self.file_name = file_name
        self.source_path = p_join(directory, file_name)
        self.target_path = p_join(DUMP_DATA_DIR, file_name + '.gz')

    def run(self):
        print("Dumpimg '{}' to '{}'".format(self.source_path, self.target_path))
        self.dump()
        source_size = human_size(self.source_path)
        target_size = human_size(self.target_path)
        print("Done. File was compressed from '{}' to '{}'.".format(source_size, target_size))

    @timeit
    def dump(self):
        with open(self.source_path, 'rb') as f_in:
            with open(self.target_path, 'wb') as f_out:
                with gzip.GzipFile(self.file_name, 'wb', fileobj=f_out) as f_out_gz:
                    shutil.copyfileobj(f_in, f_out_gz)