import json
import os

import pandas as pd
import pandas_profiling

from src.constants.csv import DEFAULT_SEPARATOR
from src.constants.directories import ROOTED_RAW_DATA_DIR, ROOTED_CLEANED_DATA_DIR
from src.utils.dataframe_io import read_csv_by_chunk, write_df_chunk_iterator, get_csv_columns
from src.utils.utils import prefix, timeit

PROFILE_REPORT_DIR_PATH = os.path.join('reports', 'raw', 'profile')


class Cleaner:
    def __init__(self, csv_name,
                 input_separator=DEFAULT_SEPARATOR,
                 input_csv_dir=ROOTED_RAW_DATA_DIR,
                 output_csv_dir=ROOTED_CLEANED_DATA_DIR,
                 keep_and_rename_columns=None,
                 assign_columns_with_functions=None,
                 piped_transformation=None,
                 additional_columns_from_piped_transformations=None,
                 columns_to_drop=None,
                 validators=None,
                 ):
        self.csv_name = csv_name
        self.input_separator = input_separator
        self.input_csv_dir = input_csv_dir
        self.input_csv_path = os.path.join(input_csv_dir, self.csv_name)
        self.output_csv_dir = output_csv_dir
        self.output_csv_path = os.path.join(output_csv_dir, self.csv_name)

        self.keep_and_rename_columns = keep_and_rename_columns
        self.assign_columns_with_functions = assign_columns_with_functions or dict()
        self.piped_transformations = piped_transformation or list()
        self.additional_columns_from_piped_transformations = additional_columns_from_piped_transformations or list()
        self.columns_to_drop = columns_to_drop or list()

        self.validators = validators or list()

    def profile_raw_csv(self):
        """ Create an html report on raw data """

        report_path = os.path.join(PROFILE_REPORT_DIR_PATH, prefix(self.csv_name) + '.html')

        print("Profiling raw '{}'. A report will be created in file '{}'".format(self.csv_name, report_path))
        df = pd.read_csv(self.input_csv_path, sep=self.input_separator)
        profile_report = pandas_profiling.ProfileReport(df, check_correlation=False)
        profile_report.to_file(report_path)

    def print_base_rename_columns_dict(self):
        """ Helper function to write a new cleaner from a raw csv file. """

        print("Print a default rename group dict for {}".format(self.csv_name))
        print(json.dumps(self.default_rename_columns_dict, sort_keys=True, indent=4, ensure_ascii=False))

    @property
    def default_rename_columns_dict(self):
        return {column: column for column in self.input_csv_columns}

    @property
    def input_csv_columns(self):
        return get_csv_columns(self.input_csv_path, separator=self.input_separator)

    @timeit
    def run(self):
        print("Cleaning", self.input_csv_path)
        self.keep_and_rename_columns = self.keep_and_rename_columns or self.default_rename_columns_dict
        print("Cleaned file will have the following columns : {}".format(self._cleaned_csv_columns))
        write_df_chunk_iterator(self.output_csv_path, self.output_df_chunk_iterator)
        print("Cleaned file is written ", self.output_csv_path)

    def output_df_chunk_iterator(self):
        number_of_cleaned_lines = 0
        for raw_df_chunk in read_csv_by_chunk(self.input_csv_path,
                                              separator=self.input_separator,
                                              columns_to_read=self.keep_and_rename_columns.keys()):
            cleaned_df_chunk = (raw_df_chunk
                                .rename(columns=self.keep_and_rename_columns)
                                .assign(**self.assign_columns_with_functions)
                                )
            for transformation in self.piped_transformations:
                cleaned_df_chunk = cleaned_df_chunk.pipe(transformation)

            cleaned_df_chunk = cleaned_df_chunk.drop(columns=self.columns_to_drop)
            self._validate(cleaned_df_chunk)
            number_of_cleaned_lines += len(cleaned_df_chunk)
            print("  {} lines cleaned.".format(number_of_cleaned_lines))
            yield cleaned_df_chunk

    @property
    def _cleaned_csv_columns(self):
        columns = set(self.keep_and_rename_columns.values())
        columns |= self.assign_columns_with_functions.keys()
        columns |= set(self.additional_columns_from_piped_transformations)
        columns -= set(self.columns_to_drop)
        return sorted(list(columns))

    def _validate(self, df):
        for is_valid in [self.has_correct_columns] + self.validators:
            assert is_valid(df), "'{}' was false for a cleaned dataframe chunk.".format(is_valid.__name__)

    def has_correct_columns(self, df):
        return set(self._cleaned_csv_columns) == set(df.columns)
