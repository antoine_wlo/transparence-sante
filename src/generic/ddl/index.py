from src.constants.postgres import INDEX
from src.generic.ddl.abstract import DDL


class IndexDDL(DDL):
    DDL_ENTITY = INDEX
    NAME_TEMPLATE = "{table}_index_on_{index_column}"
    DDL_TEMPLATE = """
CREATE INDEX IF NOT EXISTS "{name}"
ON public."{table}" ("{index_column}");
"""

    def __init__(self, table, index_column):
        super(IndexDDL, self).__init__()
        self.table = table
        self.index_column = index_column
        self.name = self.NAME_TEMPLATE.format(**self.__dict__)

    @property
    def query(self):
        return self.DDL_TEMPLATE.format(**self.__dict__)


class SearchIndexDDL(DDL):
    DDL_ENTITY = INDEX
    NAME_TEMPLATE = "{table}_prefix_index_on_{search_column}"
    DDL_TEMPLATE = """
CREATE INDEX IF NOT EXISTS "{name}"
ON public."{table}" (lower("{search_column}") text_pattern_ops, "{search_column}");
"""

    def __init__(self, table, search_column):
        super(SearchIndexDDL, self).__init__()
        self.table = table
        self.search_column = search_column
        self.name = self.NAME_TEMPLATE.format(**self.__dict__)

    @property
    def query(self):
        ddl = self.DDL_TEMPLATE.format(**self.__dict__)
        ddl += IndexDDL(self.table, self.search_column).query
        return ddl

