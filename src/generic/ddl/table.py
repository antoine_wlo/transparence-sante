from src.constants.column import ID
from src.constants.postgres import TABLE
from src.generic.ddl.abstract import VacuumedDDL
from src.utils.utils import quoted


class TableDDL(VacuumedDDL):
    DDL_ENTITY = TABLE
    DEFAULT_COLUMN_TYPE = 'TEXT'
    AGGREGATE_COLUMN_TEMPLATE = "total_{value}"
    DDL_TEMPLATE = """
CREATE TABLE "{name}" (
{columns_lines}
);
"""
    COLUMN_LINE_TEMPLATE = "    {quoted_column_name} {column_type}"

    def __init__(self, name, columns, columns_with_non_default_type):
        super(TableDDL, self).__init__()
        self.name = name
        self.columns = columns
        self.columns_with_non_default_type = columns_with_non_default_type

    @property
    def query(self):
        return self.DDL_TEMPLATE.format(name=self.name, columns_lines=self._columns_lines)

    @property
    def _columns_lines(self):
        columns_lines = [self.COLUMN_LINE_TEMPLATE.format(quoted_column_name=quoted(ID),
                                                          column_type='SERIAL PRIMARY KEY')]
        for column_name in self.columns:
            column_type = self.columns_with_non_default_type.get(column_name, self.DEFAULT_COLUMN_TYPE)
            columns_lines.append(
                self.COLUMN_LINE_TEMPLATE.format(quoted_column_name=quoted(column_name),
                                                 column_type=column_type))
        return ',\n'.join(columns_lines)
