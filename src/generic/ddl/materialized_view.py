from settings import POSTGRES_WORK_MEM
from src.constants.postgres import MATERIALIZED_VIEW
from src.generic.ddl.abstract import VacuumedDDL
from src.generic.ddl.index import SearchIndexDDL, IndexDDL


class MaterializedViewDDL(VacuumedDDL):
    DDL_ENTITY = MATERIALIZED_VIEW

    DDL_TEMPLATE = """
SET work_mem = '{work_mem}'; -- allow HashAggregate rather than a slow GroupAggregate
CREATE MATERIALIZED VIEW "{name}" AS ({select_query}
);
RESET work_mem;
"""

    def __init__(self, name, select_query, search_indices):
        super().__init__()
        self.name = name
        self.select_query = select_query
        self.search_indices = search_indices

    @property
    def query(self):
        ddl = self.DDL_TEMPLATE.format(work_mem=POSTGRES_WORK_MEM, **self.__dict__)
        for search_index in self.search_indices:
            ddl += SearchIndexDDL(self.name, search_index).query
        return ddl


class AggregatedMaterializedViewDDL(MaterializedViewDDL):
    AGGREGATED_VALUE_TEMPLATE = "{aggregation}_{value}"
    NAME_TEMPLATE = "{table}_{aggregation}_{value}_by_{group_column}"

    SELECT_QUERY_TEMPLATE = """
    SELECT
        "{group_column}",
        {aggregation}("{value}") AS "{aggregated_value}"
    FROM "{table}"
    WHERE (
        ("{group_column}" IS NOT NULL)
        AND ("{value}" IS NOT NULL)
    )
    GROUP BY "{group_column}"
    ORDER BY ({aggregation}("{value}")) DESC"""

    def __init__(self, table, value, group_column, aggregation="sum"):
        self.table = table
        self.aggregation = aggregation
        self.value = value
        self.group_column = group_column
        self.aggregated_value = self.AGGREGATED_VALUE_TEMPLATE.format(**self.__dict__)
        super().__init__(
            name=self.NAME_TEMPLATE.format(**self.__dict__),
            select_query=self.SELECT_QUERY_TEMPLATE.format(**self.__dict__),
            search_indices=[self.group_column]
        )

    @property
    def query(self):
        ddl = self.DDL_TEMPLATE.format(work_mem=POSTGRES_WORK_MEM, **self.__dict__)
        ddl += SearchIndexDDL(self.name, self.group_column).query
        ddl += IndexDDL(self.name, self.aggregated_value).query
        return ddl
