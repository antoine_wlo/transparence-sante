# Common to all files
ID = 'id'

# Common to Health directory & 3 declarations files
PHYSICAL_PERSON_ID = 'identifiant'
NAME = 'nom'
SURNAME = 'prénom'
NAME_SURNAME = NAME + '_' + SURNAME
PROFESSION = "profession"
SPECIALTY = "specialité"

# Common to Company directory & 3 declarations files
COMPANY_ID = "identifiant_entreprise"

# Specific to Company directory file
COUNTRY = "pays"
SECTOR = "secteur"
ADDRESS = "adresse"
POSTAL_CODE = "code_postal"
CITY = "ville"
COUNTRY_CODE = "code_pays"

# Specific to 3 declarations files
# - common declaration cleaner

BENEFICIARY_CATEGORY = 'catégorie_beneficiaire'
SUBSIDIARY_COMPANY = 'filiale_déclarante'
COMPANY = "entreprise_émmetrice"
DECLARATION_TYPE = 'type_declaration'
ID_TYPE = 'type_identifiant'
IS_AMOUNT_MASKED = 'montant_masque'
BENEFICIARY_ORGANIZATION_NAME = 'structure_bénéficiaire'

# - specific to each declaration cleaner, but present in all 3 declaration files
LINE_ID = 'ligne_identifiant'
DECLARATION_ID = 'declaration_id'
DATE = 'date'
AMOUNT = 'montant_ttc'
CONVENTION_ID = 'identifiant_convention'
DETAIL = 'detail'

# - specific to convention
RAW_COLUMN_CONVENTION_OBJECT = 'conv_objet'
RAW_COLUMN_CONV_OBJECT_OTHER = 'conv_objet_autre'
RAW_EVENT_NAME = 'conv_manifestation_nom'
RAW_EVENT_DATE = 'conv_manifestation_date'
RAW_EVENT_PLACE = 'conv_manifestation_lieu'
RAW_EVENT_ORGANIZER = 'conv_manifestation_organisateur'
RAW_COLUMNS_CONVENTION = [
    RAW_COLUMN_CONVENTION_OBJECT,
    RAW_COLUMN_CONV_OBJECT_OTHER,
    RAW_EVENT_NAME,
    RAW_EVENT_DATE,
    RAW_EVENT_PLACE,
    RAW_EVENT_ORGANIZER,
]

NUMBER_LINKED_REMUNERATIONS = 'nombre_remunerations_liees'
NUMBER_LINKED_ADVANTAGES = 'nombre_avantages_lies'
TOTAL_AMOUNT_LINKED_REMUNERATIONS = 'montant_total_remunerations_liees'
TOTAL_AMOUNT_LINKED_ADVANTAGES = 'montant_total_avantages_lies'
TOTAL_AMOUNT_LINKED_DECLARATIONS = 'montant_total_declarations_liees'
TOTAL_AMOUNT_CONVENTION = 'montant_total_convention'
DECLARED_AMOUNT_CONVENTION = 'montant_declare_convention'

