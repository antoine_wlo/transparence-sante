from os.path import join as p_join

from settings import DATA_ROOT_DIR

DATA = 'data'
TEST = 'tests'
EXPECTED = 'expected'
ACTUAL = 'actual'

RAW_DATA_DIR = p_join(DATA, 'raw')
CLEANED_DATA_DIR = p_join(DATA, 'cleaned')
JOINED_DATA_DIR = p_join(DATA, 'joined')
DUMP_DATA_DIR = p_join(DATA, 'dump')

ROOTED_RAW_DATA_DIR = p_join(DATA_ROOT_DIR, RAW_DATA_DIR)
ROOTED_CLEANED_DATA_DIR = p_join(DATA_ROOT_DIR, CLEANED_DATA_DIR)
ROOTED_JOINED_DATA_DIR = p_join(DATA_ROOT_DIR, JOINED_DATA_DIR)

SQL = 'sql'
DDL_DIR = p_join(SQL, 'ddl')
TEST_DDL_DIR = p_join(TEST, SQL, 'ddl')
