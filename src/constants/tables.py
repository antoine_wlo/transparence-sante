DECLARATION_REMUNERATION = 'declaration_remuneration'
DECLARATION_ADVANTAGE = 'declaration_avantage'
DECLARATION_CONVENTION = 'declaration_convention'
HEALTH_DIRECTORY = 'annuaire'
COMPANY_DIRECTORY = 'entreprise'

# View
DECLARATION = 'declaration'

# Materialized view
DUPLICATES_AMOUNT_ADVANTAGE_CONVENTION = "doublons_montants_avantage_convention"
