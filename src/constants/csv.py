# Separators
SEMICOLON_SEPARATOR = ';'
COMMA_SEPARATOR = ','
DEFAULT_SEPARATOR = SEMICOLON_SEPARATOR

# CSV names
COMPANY_CSV = 'entreprise.csv'
REMUNERATION_CSV = 'declaration_remuneration.csv'
ADVANTAGE_CSV = 'declaration_avantage.csv'
CONVENTION_CSV = 'declaration_convention.csv'

PARENT_COMPANY_CSV = 'societe_mere.csv'

HEALTH_DIRECTORY_CSV = "ExtractionMonoTable_CAT18_ToutePopulation.csv"

# CSV lists
DECLARATION_CSV_LIST = [REMUNERATION_CSV,
                        ADVANTAGE_CSV,
                        CONVENTION_CSV]

ETALAB_CSV_LIST = [COMPANY_CSV] + DECLARATION_CSV_LIST
CSV_LIST = [HEALTH_DIRECTORY_CSV] + ETALAB_CSV_LIST
