import os

import pandas as pd

from settings import ROWS_LIMIT, CSV_CHUNK_SIZE
from src.constants.csv import DEFAULT_SEPARATOR


def read_csv_by_chunk(csv_path, separator, columns_to_read=None):
    if not os.path.exists(csv_path):
        raise FileNotFoundError("CSV '{} does not exists")
    return pd.read_csv(csv_path,
                       sep=separator,
                       usecols=columns_to_read,
                       dtype='str',
                       chunksize=CSV_CHUNK_SIZE,
                       nrows=ROWS_LIMIT)


def write_df_chunk_iterator(csv_path, df_chunk_iterator):
    if os.path.exists(csv_path):
        os.remove(csv_path)

    write_header = True
    for df_chunk in df_chunk_iterator():
        df_chunk.to_csv(
            csv_path,
            index=False,
            sep=DEFAULT_SEPARATOR,
            mode='a',
            header=write_header,
            columns=sorted(df_chunk.columns)
        )
        write_header = False


def get_csv_columns(csv_path, separator=DEFAULT_SEPARATOR):
    if os.path.exists(csv_path):
        return list(pd.read_csv(csv_path, sep=separator, nrows=1).columns)
    else:
        raise FileNotFoundError("CSV file '{}' does not exists".format(csv_path))
