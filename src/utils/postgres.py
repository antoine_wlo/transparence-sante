import os

from sqlalchemy import create_engine, text

from settings import POSTGRES_HOST, POSTGRES_PORT, POSTGRES_PASSWORD
from src.constants.connexions import POSTGRES_USER
from src.constants.directories import SQL
from src.utils.utils import timeit

POSTGRES_URL = "postgresql://{user}:{password}@{host}:{port}".format(user=POSTGRES_USER,
                                                                     password=POSTGRES_PASSWORD,
                                                                     host=POSTGRES_HOST,
                                                                     port=POSTGRES_PORT)


def create_postgres_engine():
    # Without AUTOCOMMIT's isolation_level, "CREATE MATERIALIZED VIEW" query silently fails
    # when there is a first line "SET work_mem … ;"
    return create_engine(POSTGRES_URL, isolation_level='AUTOCOMMIT')


@timeit
def execute_text_query(engine, raw_query, verbose=True):
    query = text(raw_query)
    if verbose:
        print("Execute query : {}".format(query))
    result = engine.execute(query)
    if result.returns_rows:
        for row in result:
            print(row)


def setup_monitoring():
    engine = create_postgres_engine()
    setup_monitoring_path = os.path.join(SQL, "setup_monitoring.sql")
    with open(setup_monitoring_path, 'r') as f:
        query = f.read()
    print("Setup monitoring in PostgreSQL database")
    execute_text_query(engine, query, verbose=False)


if __name__ == '__main__':
    setup_monitoring()
