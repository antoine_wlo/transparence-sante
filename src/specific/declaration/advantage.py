from src.constants import column
from src.constants.categorical import ADVANTAGE, CONVENTION
from src.constants.csv import ADVANTAGE_CSV
from src.specific.declaration.common import DeclarationCleaner, DeclarationUploader
from src.specific.declaration.functions import get_declaration_id_builder

cleaner = DeclarationCleaner(
    csv_name=ADVANTAGE_CSV,
    keep_and_rename_columns={
        'avant_date_signature': column.DATE,
        'avant_montant_ttc': column.AMOUNT,
        'avant_convention_lie': column.CONVENTION_ID,
        'avant_nature': column.DETAIL,
    },
    assign_columns_with_functions={
        column.DECLARATION_TYPE: ADVANTAGE,
        column.DECLARATION_ID: get_declaration_id_builder(ADVANTAGE, column.LINE_ID),
        column.CONVENTION_ID: get_declaration_id_builder(CONVENTION, column.CONVENTION_ID)
    },
    columns_to_drop=list()
)
uploader = DeclarationUploader(ADVANTAGE_CSV)

if __name__ == '__main__':
    cleaner.run()