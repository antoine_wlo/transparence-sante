import re
import string
import unicodedata

from src.constants import column

AUTHORIZED_CHARACTERS = string.ascii_letters + ' '

MINIMAL_YEAR = 1970
MAXIMAL_YEAR = 2020
MINIMAL_YEAR_DECADE = 70
MAXIMAL_YEAR_DECADE = 20


def recipient_name_surname(df):
    name = df[column.NAME].pipe(normalize_string_series).str.upper()
    surname = df[column.SURNAME].pipe(normalize_string_series).str.title()
    name_surname = name.str.cat(surname.values, sep=' ')
    return name_surname.str.strip()


def normalize_string_series(series):
    return (series
            .fillna('').map(str)
            .map(replace_accents)
            .map(replace_special_characters)
            .str.split().str.join(' ')
            )


def replace_accents(s):
    return ''.join(c for c in unicodedata.normalize('NFD', s)
                   if unicodedata.category(c) != 'Mn')


def replace_special_characters(s):
    return re.sub('[^{}]'.format(AUTHORIZED_CHARACTERS), ' ', s)


def corrected_date(df):
    def corrected_year(year):
        year_decade = year[2:]
        if MINIMAL_YEAR_DECADE < int(year_decade):
            return '19' + year_decade
        elif int(year_decade) < MAXIMAL_YEAR_DECADE:
            return '20' + year_decade
        else:
            return '2000'

    s_date = df[column.DATE]
    s_day_month = s_date.str[:6]
    s_year = s_date.str[6:].map(corrected_year)
    return s_day_month + s_year


def is_year_in_correct_range(df):
    def is_date_string_year_in_correct_range(date_string):
        year = int(date_string[6:])
        return MINIMAL_YEAR <= year <= MAXIMAL_YEAR

    return df[column.DATE].map(is_date_string_year_in_correct_range).all()


def get_declaration_id_builder(declaration_type, declaration_id_column):
    def build_declaration_id(df):
        return (declaration_type + ' | ' +
                df[column.COMPANY_ID] + ' | ' +
                df[declaration_id_column]
                )
    return build_declaration_id