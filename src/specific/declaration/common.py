from src.constants import column, tables
from src.constants.column import DATE, AMOUNT
from src.generic.cleaner import Cleaner
from src.generic.ddl.index import IndexDDL, SearchIndexDDL
from src.generic.ddl.materialized_view import AggregatedMaterializedViewDDL
from src.generic.uploader import Uploader
from src.specific.company import parent_company
from src.specific.declaration.functions import recipient_name_surname, corrected_date, is_year_in_correct_range
from src.utils.utils import merge_dict

DECLARATION_TABLES = [tables.DECLARATION_ADVANTAGE, tables.DECLARATION_CONVENTION, tables.DECLARATION_REMUNERATION]

RENAME_COLUMNS = {
    'ligne_identifiant': column.LINE_ID,
    'benef_nom': column.NAME,
    'benef_prenom': column.SURNAME,
    'benef_identifiant_valeur': column.PHYSICAL_PERSON_ID,
    'entreprise_identifiant': column.COMPANY_ID,
    'denomination_sociale': column.SUBSIDIARY_COMPANY,
    'categorie': column.BENEFICIARY_CATEGORY,
    'identifiant_type': column.ID_TYPE,
    'benef_speicalite_libelle': column.SPECIALTY,
    'qualite': column.PROFESSION,
    'benef_denomination_sociale': column.BENEFICIARY_ORGANIZATION_NAME,
}

ASSIGN_BEFORE = {
    column.NAME_SURNAME: recipient_name_surname,
    column.COMPANY: parent_company
}

ASSIGN_AFTER = {
    column.DATE: corrected_date
}

COLUMNS_TO_DROP = [column.NAME, column.SURNAME, column.LINE_ID]

VALIDATORS = [is_year_in_correct_range]

DECLARATION_COLUMN_TYPE_DICT = {DATE: 'DATE', AMOUNT: 'NUMERIC'}

DECLARATION_SEARCH_COLUMNS = [
    column.NAME_SURNAME,
    column.PHYSICAL_PERSON_ID,
    # column.SUBSIDIARY_COMPANY,
    column.COMPANY,
    column.BENEFICIARY_ORGANIZATION_NAME,
    # column.BENEFICIARY_CATEGORY,
    # column.SPECIALTY
    # column.CONVENTION_ID
]

DECLARATION_AGGREGATE_VALUE_BY_GROUP = [
    (column.AMOUNT, column.NAME_SURNAME),
    (column.AMOUNT, column.SUBSIDIARY_COMPANY),
    (column.AMOUNT, column.COMPANY),
]


class DeclarationCleaner(Cleaner):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.keep_and_rename_columns = merge_dict([RENAME_COLUMNS, self.keep_and_rename_columns])
        self.assign_columns_with_functions = merge_dict([ASSIGN_BEFORE, self.assign_columns_with_functions,
                                                         ASSIGN_AFTER])
        self.columns_to_drop = COLUMNS_TO_DROP + self.columns_to_drop
        self.validators = VALIDATORS + self.validators


class DeclarationUploader(Uploader):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.columns_with_non_default_type = DECLARATION_COLUMN_TYPE_DICT


indices = [
    IndexDDL(table, column.COMPANY_ID)
    for table in DECLARATION_TABLES
]

search_indices = [
    SearchIndexDDL(table, column_name)
    for column_name in DECLARATION_SEARCH_COLUMNS
    for table in DECLARATION_TABLES
]

materialized_views = [
    AggregatedMaterializedViewDDL(tables.DECLARATION, value, column)
    for value, column in DECLARATION_AGGREGATE_VALUE_BY_GROUP
]

if __name__ == '__main__':
    for ddl in materialized_views:
        ddl.run()
