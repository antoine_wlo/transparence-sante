from src.constants import column
from src.constants import tables
from src.constants.csv import HEALTH_DIRECTORY_CSV
from src.constants.tables import HEALTH_DIRECTORY
from src.generic.cleaner import Cleaner
from src.generic.ddl.index import SearchIndexDDL
from src.generic.uploader import Uploader

cleaner = Cleaner(
    csv_name=HEALTH_DIRECTORY_CSV,
    keep_and_rename_columns={
        # Type d'identifiant PP": "Type d'identifiant PP",
        "Identifiant PP": column.PHYSICAL_PERSON_ID,
        # Identification nationale PP": "Identification nationale PP",
        # Code civilité exercice": "Code civilité exercice",
        "Libellé civilité exercice": "civilité_exercice",
        "Nom d'exercice": column.NAME,
        "Prénom d'exercice": column.SURNAME,
        # Code profession": "Code profession",
        "Libellé profession": column.PROFESSION,
        # Code catégorie professionnelle": "Code catégorie professionnelle",
        "Libellé catégorie professionnelle": "catégorie_professionnelle",
        # Code savoir-faire": "Code savoir-faire",
        "Libellé savoir-faire": column.SPECIALTY,
        # Code type savoir-faire": "Code type savoir-faire",
        "Libellé type savoir-faire": "type_spécialité",
        "Numéro SIRET site": "siret_site",
        "Numéro SIREN site": "siren_site",
        "Numéro FINESS site": "finess_site",
        "Numéro FINESS établissement juridique": "finess_etablissement_juridique",
        "Raison sociale site": "raison_sociale_site",
        "Enseigne commerciale site": "enseigne_commerciale_site",
        "Identifiant structure": "identifiant_structure",
        "Complément destinataire (coord. structure)": "complément_destinataire_structure",
        "Complément point géographique (coord. structure)": "complément_point_géographique_structure",
        "Numéro Voie (coord. structure)": "numéro_voie_structure",
        "Indice répétition voie (coord. structure)": "indice_répétition_voie_structure",
        "Code type de voie (coord. structure)": "code_type_de_voie_structure",
        "Libellé type de voie (coord. structure)": "libellé_type_de_voie_structure",
        "Libellé Voie (coord. structure)": "libellé_voie_structure",
        "Mention distribution (coord. structure)": "mention_distribution_structure",
        "Bureau cedex (coord. structure)": "bureau_cedex_structure",
        "Code postal (coord. structure)": "code_postal_structure",
        # Code commune (coord. structure)": "Code_commune_structure",
        "Libellé commune (coord. structure)": "libellé_commune_structure",
        # Code pays (coord. structure)": "Code_pays_structure",
        "Libellé pays (coord. structure)": "libellé_pays_structure",
        "Téléphone (coord. structure)": "téléphone_structure",
        "Téléphone 2 (coord. structure)": "téléphone_2_structure",
        "Télécopie (coord. structure)": "télécopie_structure",
        "Adresse e-mail (coord. structure)": "adresse_email_structure",
        "Adresse BAL MSSanté": "adresse_bal_mssanté",
        # Unnamed: 41": "Unnamed: 41",
    }
)

uploader = Uploader(HEALTH_DIRECTORY_CSV, table_name=HEALTH_DIRECTORY)
HEALTH_DIRECTORY_SEARCH_COLUMNS = [column.NAME, column.SURNAME, column.PHYSICAL_PERSON_ID]
health_directory_search_indices = [
    SearchIndexDDL(tables.HEALTH_DIRECTORY, column_name)
    for column_name in HEALTH_DIRECTORY_SEARCH_COLUMNS
]

if __name__ == '__main__':
    cleaner.run()