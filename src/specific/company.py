import os

import pandas as pd

from src.constants import tables, column
from src.constants.csv import COMPANY_CSV, COMMA_SEPARATOR, PARENT_COMPANY_CSV
from src.constants.directories import ROOTED_RAW_DATA_DIR
from src.generic.cleaner import Cleaner
from src.generic.ddl.index import SearchIndexDDL
from src.generic.uploader import Uploader

ADDRESS_LINE_1 = "adresse_1"
ADDRESS_LINE_2 = "adresse_2"
ADDRESS_LINE_3 = "adresse_3"
ADDRESS_LINE_4 = "adresse_4"
ADDRESS_LINES = [ADDRESS_LINE_1, ADDRESS_LINE_2, ADDRESS_LINE_3, ADDRESS_LINE_4]

COMPANY_SEARCH_COLUMNS = [
    column.COUNTRY,
    column.SECTOR,
    column.COMPANY_ID,
    column.COMPANY
]


def address(df):
    s_address = df[ADDRESS_LINE_1].fillna('').str.rstrip()
    for line in ADDRESS_LINES[1:]:
        s_address += (', ' + df[line]).fillna('').str.rstrip()
    return s_address


def parent_company(df):
    csv_path = os.path.join(ROOTED_RAW_DATA_DIR, PARENT_COMPANY_CSV)
    source_column = column.SUBSIDIARY_COMPANY

    referential_df = pd.read_csv(csv_path, sep=COMMA_SEPARATOR, dtype='str')
    mapping = (referential_df.set_index("denomination_sociale").to_dict()["societe_mere"])
    return (df[source_column]
            .map(mapping, na_action='ignore')
            .fillna(df[source_column])
            )


cleaner = Cleaner(
    csv_name=COMPANY_CSV,
    input_separator=COMMA_SEPARATOR,
    keep_and_rename_columns={
        "identifiant": column.COMPANY_ID,
        "pays_code": column.COUNTRY_CODE,
        "pays": column.COUNTRY,
        "secteur": column.SECTOR,
        "denomination_sociale": column.SUBSIDIARY_COMPANY,
        "adresse_1": ADDRESS_LINE_1,
        "adresse_2": ADDRESS_LINE_2,
        "adresse_3": ADDRESS_LINE_3,
        "adresse_4": ADDRESS_LINE_4,
        "code_postal": column.POSTAL_CODE,
        "ville": column.CITY,
    },
    assign_columns_with_functions={
        column.ADDRESS: address,
        column.COUNTRY_CODE: lambda df: df[column.COUNTRY_CODE].str.lstrip('[').str.rstrip(']'),
        column.COMPANY: parent_company
    },
    columns_to_drop=ADDRESS_LINES
)

uploader = Uploader(COMPANY_CSV)

search_indices = [
    SearchIndexDDL(tables.COMPANY_DIRECTORY, column_name)
    for column_name in COMPANY_SEARCH_COLUMNS
]

if __name__ == '__main__':
    cleaner.run()
