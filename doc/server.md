
## Server administration 

### From PC

- Setup ssh without password : ssh-copy-id
- Add an alias in /etc/hosts to server ip

### On server

- htop


    sudo apt-get install htop

- ssh on port 443 to bypass public wifi's firewalls

Removed as it uses port used by https


    nano /etc/ssh/sshd_config
    # Add lines
    Port 443 
    Port 22
    
    service ssh restart
   

## Server installation

- [Install Docker](https://docs.docker.com/install/linux/docker-ce/debian/#install-docker-ce-1)
- [Install Docker-Compose](https://docs.docker.com/compose/install/#install-compose)
- [Install git](https://www.digitalocean.com/community/tutorials/how-to-install-git-on-debian-8)
- Generate ssh key (ssh-keygen -t rsa), add it to GitLab 
- Download project


    git clone git@gitlab.com:eurosfordocs/transparence-sante.git
    cd transparence-sante

- Setup Python 3.6.5 (necessary with type's hints)

Following this comment https://unix.stackexchange.com/a/350985

    cd ~
    mkdir pythonroot opt app
    cd opt
    wget https://www.python.org/ftp/python/3.6.5/Python-3.6.5.tgz
    tar -xvf Python-3.6.5.tgz
    cd Python-3.6.5
    sudo apt-get update
    sudo apt-get install -y make build-essential libssl-dev zlib1g-dev   
    sudo apt-get install -y libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm 
    sudo apt-get install -y libncurses5-dev  libncursesw5-dev xz-utils tk-dev
    ./configure --prefix="$HOME"/pythonroot --with-ensurepip=install 
    make
    make install
    
    cd ~/transparence-sante
    virtualenv venv --python ~/pythonroot/bin/python3
    source venv/bin/activate
    pip3 install -r  requirements.txt


- Add secrets file to server


    scp config/secrets.py root@ts:/root/transparence-sante/config/


- Install project
    
    
     python main.py
     



       
