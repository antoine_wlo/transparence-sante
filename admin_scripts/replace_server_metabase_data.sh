#!/usr/bin/env bash
set -euo pipefail

host=${1:-"ts-prod"}
metabase_file_path="docker/metabase/data/metabase.db.mv.db"

echo Replacing metabase server data with file \'${metabase_file_path}\'

if [ ! -f ${metabase_file_path} ]; then
    echo "File not found! Aborting"
    exit
fi

echo This will stop Metabase on server.
echo This will DESTROY ANY DATA IN METABASE ON SERVER.

read -p "Continue (I confirm/ No)?" choice
case "$choice" in
  "I confirm") echo "Yes. Going on" ;;
  n|N|no|No ) echo "No. Aborting" ; exit ;;
  * ) echo "invalid. Aborting" ; exit ;;
esac

ssh -t root@${host} "cd transparence-sante && docker-compose stop metabase"
ssh -t root@${host} "cd transparence-sante/docker/metabase && mv data/* backup/"
scp ${metabase_file_path} root@${host}:transparence-sante/docker/metabase/data/metabase.db.mv.db
ssh root@${host} "cd transparence-sante && docker-compose up --no-deps -d metabase"
