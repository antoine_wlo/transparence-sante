#!/usr/bin/env python3
import sys

from settings import POSTGRES_HOST
from src.constants.connexions import LOCALHOST, DOCKER_COMPOSE_POSTGRES_SERVICE
from src.constants.miscellaneous import ERROR_CODE_INVALID_ARGUMENT
from src.generic.dumper import Dumper
from src.specific import health_directory, company
from src.specific.declaration import duplicate_amounts_advantage_convention
from src.specific.declaration import remuneration, advantage, convention, common as declaration, \
    view as declaration_view, join_convention_to_other_declarations
from src.specific.download import download
from src.specific.health_directory import health_directory_search_indices
from src.utils.postgres import setup_monitoring
from src.utils.utils import get_confirmation


def all_cleaners():
    return [
        health_directory.cleaner,
        remuneration.cleaner,
        advantage.cleaner,
        convention.cleaner,
        company.cleaner,
        join_convention_to_other_declarations.cleaner
    ]


def clean():
    for cleaner in all_cleaners():
        cleaner.run()


def join():
    join_convention_to_other_declarations.cleaner.run()


def all_uploaders():
    return [
        health_directory.uploader,
        company.uploader,
        remuneration.uploader,
        advantage.uploader,
        join_convention_to_other_declarations.uploader
    ]


def all_views_ddl():
    return ([declaration_view.view()] +
            declaration.materialized_views +
            [duplicate_amounts_advantage_convention.materialized_view])


def recreate_views():
    for view in all_views_ddl():
        view.reset()
        view.run()


all_indices = (
        health_directory_search_indices +
        declaration.search_indices +
        declaration.indices +
        company.search_indices
)


def reset_upload():
    for uploader in all_uploaders():
        uploader.reset()


def upload():
    for uploader in all_uploaders():
        uploader.run()
    for view in all_views_ddl():
        view.run()
    for index in all_indices:
        index.run()


def update_ddl():
    for uploader in all_uploaders():
        uploader.table_ddl.write_ddl()
    for view in all_views_ddl():
        view.write_ddl()


def dump():
    for uploader in all_uploaders():
        dumper = Dumper(directory=uploader.csv_directory, file_name=uploader.csv_name)
        dumper.run()


def process():
    clean()
    reset_upload()
    upload()


ACTION_MAPPING = {
    'download': download,
    'clean': clean,
    'join': join,
    'upload': upload,
    'reset-upload': reset_upload,
    'process': process,
    'setup-monitoring': setup_monitoring,
    'update-ddl': update_ddl,
    'recreate-views': recreate_views,
    'dump': dump,
}


def invalid_input():
    print("Main entrypoint to use eurosfordocs project\n")
    print("Usage:")
    for action_key in ACTION_MAPPING.keys():
        print("  ./main.py {}".format(action_key))
    exit(ERROR_CODE_INVALID_ARGUMENT)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        invalid_input()

    action = sys.argv[1]
    if action in ACTION_MAPPING:
        if POSTGRES_HOST not in [LOCALHOST, DOCKER_COMPOSE_POSTGRES_SERVICE]:
            get_confirmation(
                "You are going to execute action '{}' ON HOST '{}'.\nDo you confirm ?".format(action, POSTGRES_HOST)
            )
        ACTION_MAPPING[action]()
    else:
        invalid_input()
