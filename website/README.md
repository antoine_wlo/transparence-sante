Using [Jekyll](https://jekyllrb.com/docs/quickstart/), with  [minimal-mistakes theme](https://mmistakes.github.io/minimal-mistakes/).

To build website, execute 

    docker-compose up website
