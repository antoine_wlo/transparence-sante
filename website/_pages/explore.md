---
permalink: /explore/
title: Explorer
toc: true
---

<p style="text-align:center;">
<img 
    src="{{ base }}/assets/images/logo_metabase.svg" 
    alt="Logo Metabase" 
    width="200"
/>
</p>

## Metabase

EurosForDocs fournit l'interface **Metabase** pour explorer la base Transparence-Santé. 

Metabase est un outil d'analyse de données open-source, développée par une startup californienne, selon un modèle économique [freemium](https://fr.wikipedia.org/wiki/Freemium).  

L'accès à Metabase nécessite de se connecter[^authentification] :
- Soit directement via le `Sign In Google`, pour les adresses terminant par `@gmail.com`.
- Soit en demandant la création d'un compte à `contact@eurosfordocs.fr`. Vous recevrez alors un lien par email pour créer un mot de passe.

[^authentification]: L'obligation de connexion est une limitation technique. L'objectif à moyen terme est d'offrir sans connexion toutes les fonctionnalités de recherche. Les fonctionnalités d'analyse avancée demanderont elles toujours une connexion, 

Il est **impératif de lire attentivement** les quelques paragraphes qui suivent **avant** de vous connecter en bas de page. 


## License des données de la base Transparence-Santé

Les données de la base Transparence-Santé sont essentiellement libres de réutilisations, avec des restrictions précisées dans cette [licence]({{ base }}/download/Licence_reutilisation_donnees_transparence_sante.pdf).  

En utilisant Metabase, vous devenez _réutilisateur_ des données et êtes soumis à ces restrictions. En voici une _sélection_, qui ne remplace pas la lecture de la licence :
- Les réutilisations doivent avoir pour finalité la transparence des liens d’intérêts[^finalite].
- Les données ne peuvent être exploitée à titre strictement commercial. Vous n'avez par exemple pas le droit d'utiliser ces données pour du ciblage marketing des professionels de santé.
- Vous devez citer la source des données lors de réutilisations.

[^finalite]: La finalité de transparence des liens d'intérêts est une notion juridiquement floue. Cette imprécision créé un risque juridique pour le réutilisateur.

## Vos données personnelles

### Collecte de données personnelles

EurosForDocs collecte un minimum de données personnelles, et tend vers une collecte zéro ([aide](/contributing) technique bienvenue).

EurosForDocs ne collecte **aucune** donnée personnelle sur les pages accessibles sans connexion[^tiers].

[^tiers]: Nous utilisons les services de l'entreprise américaine Cloudflare (DNS, proxy-inverse, SSL, etc.) qui *affirme* [ne conserver aucune donnée personnelle](https://developers.cloudflare.com/1.1.1.1/commitment-to-privacy/).

Lorsque vous créez un compte et naviguez sur Metabase :
- Metabase stocke votre adresse email, votre nom et votre prénom[^nom].
- Metabase stocke potentiellement toutes vos actions[^activite].
- Si vous utilisez le `Sign-In Google`, Google est informé lors des connexions.
- Si vous utilisez l'authentificaction par mot de passe, Metabase stocke ce mot de passe de façon sécurisée.[^passe] 

[^nom]: Le nom et le prénom sont modifiables dans les réglages du comptes utilisateur de Metabase.

[^activite]: Il faut considérer par défaut que toutes vos actions sur Metabase _peuvent_ être tracées, comme sur la plupart des sites internet. La collecte d'informations sur l'activité des utilisateurs est intégrée à l'outil Metabase. Nous n'avons pas (encore - help needed) développé une méthode pour supprimer automatiquement ces informations. Ce problème d'anonymat est un potentiel problème de sécurité pour vous. Une réponse à ce problème arrivera lorsque les fonctionnalités de recherche seront accessibles sans connexion. 

[^passe]: Le mot de passe est [salé](https://fr.wikipedia.org/wiki/Salage_(cryptographie)) et hashé. Pour **votre** sécurité, nous vous rappelons cependant d'utiliser des mot de passe *différents* sur *tous* les sites internet. 

Vous disposez d'un [droit d'accès et de rectification](https://fr.wikipedia.org/wiki/Loi_informatique_et_libert%C3%A9s#Les_droits_essentiels_particuliers_reconnus_par_cette_loi_et_leur_exercice_concret) à vos données personnelles.

### Traitement de données personnelles

EurosForDocs utilise les adresses email et l'identité des utilisateurs de Metabase pour communiquer avec eux. Ces interactions sont manuelles et personnelles.[^email] 

[^email]: EurosForDocs n'a pas mis en place de newsletter. Utilisez les réseaux sociaux Twitter ou LinkedIn pour suivre notre activité. Nous nous réservons cependant le droit d'envoyer un courrier à l'ensemble des utilisateurs de Metabase pour communiquer des informations majeures. 

EurosForDocs utilise les données d'activités collectées par Metabase pour améliorer la plateforme.

EurosForDocs ne partage aucune information personnelle avec des tiers, sauf en cas d'obligation légale, ce qui n'est encore jamais arrivé.

_En vous connectant à Metabase, vous acceptez la collecte et le traitement de ces informations personelles._

## Utilisation de Metabase

### Première utilisation

Commencer par utiliser les tableaux de bord existants, offrant une vision par entreprise ou par bénéficiaire. 

**Filtre** : 
Utiliser les filtres présents en haut à gauche pour restreindre les résultats affichés.  
{: .notice--success}

La **recherche** n'est pas sensible à la casse (minuscule vs majuscule).
En revanche, les valeurs sélectionnées sont sensibles à la casse. 
Les *copier-coller* ne fonctionneront donc pas. 
{: .notice--warning} 

**Attention !** 
Le nettoyage des données est en cours.
Des erreurs de traitement sont probables. Des erreurs d'interprétation de votre part sont également possibles. 
Il convient pour l'instant de demander conseil avant toute publication utilisant cette plateforme.
{: .notice--danger}

### Se connecter

En vous connectant, vous déclarez avoir lu et compris les informations de cette page.

[Se connecter à Metabase](/metabase){: .btn .btn--success .btn--large}

### Utilisation avancée

Metabase permet de réaliser des analyses ad-hoc sans compétences de programmation.

Nous vous invitons à parcourir la [documentation de Metabase](https://www.metabase.com/docs/latest/getting-started.html), ou à nous contacter pour une rapide formation à distance.
