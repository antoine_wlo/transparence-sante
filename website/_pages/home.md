---
permalink: /
title: Bienvenue
layout: splash
header:
  overlay_color: "#000"
  overlay_filter: "0.2"
  overlay_image: assets/images/trust-transparency.jpg?maxwidth=573
  caption: "[Photo credit](http://www.eu-patient.eu/News/News/transparency-is-our-legitimacy-currency-lets-protect-it/)"
excerpt: "Pour la transparence du lobbying des industries de santé."
---


Le système de santé français est régulièrement secoué par des **crises sanitaires**; celle du Levothyrox depuis 2017. 

**Derrière les crises sanitaires se cachent souvent des conflits d'intérêts majeurs** entre les experts et les entreprises pharmaceutiques. Les études scientifiques et les prescriptions médicales manquent d’indépendance, influencées par une industrie discrètement omniprésente.


Depuis 2010, le scandale du Médiator a amorcé une évolution. Les industriels doivent désormais déclarer tous leurs liens d'intérêts financiers dans la base **[Transparence-Santé](https://www.transparence.sante.gouv.fr/flow/interrogationAvancee?execution=e2s1)**.

<p style="text-align:center;">
<img 
    src="assets/images/base_transparence_sante.png" 
    alt="Base Transparence Santé" 
    width="800"
/>
</p>

Le site public est malheureusement pauvre en fonctionalités, avec une ergonomie limitée. 

**Ce dispositif de Transparence est généralement perçu comme opaque**. 
 
Le collectif **Euros For Docs** simplifie l'accès à la base Transparence-Santé.

[Explorer les liens d'intérêts déclarés par les industriels](/explore){: .btn .btn--success .btn--large}

<iframe 
    src="https://www.eurosfordocs.fr/metabase/public/dashboard/0931cd9a-de38-4f17-b7d7-b05f56bf3f12" 
    frameborder="0" 
    width="100%"
    height="900" 
    allowtransparency
></iframe>

<p style="text-align:center;">
<iframe 
    src="https://docs.google.com/presentation/d/e/2PACX-1vRKdaX6NCtVpldkAy1Jl2Ojyw1UaEYhy5Xqzxlaki364zi7fy0OmT7WLtUiYy2BxWyUm3opdHQMM0uy/embed?start=false&loop=false&delayms=1000" 
    frameborder="0" 
    width="960" 
    height="569" 
    allowfullscreen="true" 
    mozallowfullscreen="true" 
    webkitallowfullscreen="true"
></iframe>
</p>
