---
permalink: /contributing/
title: Contribuer
---

<i class="far fa-hand-point-right" style="color:Tomato"></i> 
We need YOU ! 

EurosForDocs est un collectif ouvert. Toute participation est **bienvenue**, quelque soit le niveau d'implication.

Pour les parisiens, rendez-vous tous les mercredi soir à 19h au Liberté Living Lab.

<iframe 
    src="https://calendar.google.com/calendar/b/2/embed?showTitle=0&amp;showNav=0&amp;showDate=0&amp;showPrint=0&amp;showTabs=0&amp;showCalendars=0&amp;showTz=0&amp;mode=AGENDA&amp;height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=contact%40eurosfordocs.fr&amp;color=%231B887A&amp;ctz=Europe%2FParis" 
    style="border-width:0" 
    width="800" 
    height="150" 
    frameborder="0" 
    scrolling="no">
</iframe>


<i class="fab fa-osi fa-fw"></i> 
EurosForDocs est un projet [libre](https://gitlab.com/eurosfordocs/transparence-sante){:target="_blank"}.
Le code est publié sous licence GNU AGPLv3[^licence], qui garantit à quiconque la liberté de le réutiliser. 

[^licence]: Vous avez le droit de réutiliser, étudier, dupliquer et modifier le code source du projet. La caractéristique [copyleft](https://fr.wikipedia.org/wiki/Copyleft) de la licence impose cependant que toute évolution du code doit être publiée sous les mêmes conditions, sans restrictions de droits.


Voir la liste des [contributeurs au code source](https://gitlab.com/eurosfordocs/transparence-sante/graphs/master){:target="_blank"}. 

<i class="fab fa-connectdevelop fa-fw" style="color:DarkBlue"></i>
Le projet, initié par [Pierre-Alain Jachiet](https://www.linkedin.com/in/pierrealainjachiet/){:target="_blank"}, a vocation a être porté par un large collectif ouvert.

Quelques soient votre temps et vos compétences, votre contribution sera précieuse :
- <i class="fas fa-laptop-code fa-fw"></i>
**Développement** de la plateforme[^dev] (cf [GitLab Issues](https://gitlab.com/eurosfordocs/transparence-sante/issues))
- <i class="fas fa-balance-scale fa-fw"></i>
 Limitation des risques **juridiques**, et analyse des lois
- <i class="fas fa-edit fa-fw"></i> 
**Rédaction** et relecture de contenu
- <i class="fas fa-bullhorn fa-fw"></i>
**Communication**
- <i class="fas fa-map-signs fa-fw"></i>
**UX** du site et **Graphisme** d'un logo  
- <i class="fas fa-capsules fa-fw"></i>
Connaissances sur le **lobbying** des industriels de santé
- <i class="fa fa-address-book fa-fw"></i>
Contacts et partenariats avec des **acteurs institutionnels** (services de l'État, conseils des ordes, CNIL, etc)

[^dev]: Un effort important doit être finalisé pour simplifier l'installation de la plateforme, et faciliter les contributions. Les chantiers à venir sont multiples et passionnants : développement d'un site web dynamique, nettoyage et analyses de données, DevOps, Craftmanship, sécurité.

<i class="fas fa-envelope fa-fw" style="color:DarkBlue"></i>
Écrivez à [`contact@eurosfordocs.fr`](mailto:contact@eurosfordocs.fr).

<i class="fab fa-angellist fa-fw" style="color:Green"></i> 
Toutes vos remarques, idées, encouragements ou contacts sont les bienvenues !

<i class="fas fa-euro-sign fa-fw" style="color:Gold"></i>
Vous pouvez également financer le projet via ce [collectif ouvert](https://opencollective.com/eurosfordocs){:target="_blank"}, qui offre une comptabilité totalement transparente en accord avec nos principes.

Le projet est bénévole. Les besoins financiers sont restreints à l'hébergement du site et à organiser des rencontres.  

Votre soutien - même symbolique - permettra d'allonger la liste des contributeurs, afin de démontrer un large intérêt citoyen pour le sujet.
