---
permalink: /context/
title: Histoire de la base Transparence-Santé
toc: true
---

## La transparence, pour éviter un nouveau Médiator

En 2010, le scandale du [Mediator](https://fr.wikipedia.org/wiki/Affaire_du_Mediator) éclate. 
Faisant suite à une série de crises sanitaires, il révèle des failles majeures dans toute la chaîne de sécurité des médicaments[^senat-dysfonctionnement].

La corruption de l'expertise sanitaire par le lobby pharmaceutique[^senat-independance] apparaît comme une cause récurrente de ces dysfonctionnements.

Le 29 décembre 2011, la loi post-Médiator[^loi-post-mediator] - dite Bertrand - est promulguée. 

Pour lutter contre les conflits d'intérêts, sa disposition phare est de rendre transparents les liens entre industriels et professionnels de santé. Désormais, les entreprises devront déclarer le détail des dons, subventions et contrats sur un site internet public. 

Le 26 Juin 2014, 4 ans après le scandale du Médiator, la base de données Transparence-Santé est officiellement lancée. 

[^senat-dysfonctionnement]: Rapport du Sénat, [partie 1.II](http://www.senat.fr/rap/r10-675-1/r10-675-18.html#toc162): UNE SITUATION RÉVÉLATRICE DES DYSFONCTIONNEMENTS DU DISPOSITIF DE SÉCURITÉ SANITAIRE
 
[^senat-independance]: _"le domaine sanitaire est particulièrement exposé au risque de conflits d'intérêts en raison d'enjeux financiers considérables et du lobbying des laboratoires pharmaceutiques."_ Rapport du Sénat, [partie 1.II.B.2.b](http://www.senat.fr/rap/r10-675-1/r10-675-19.html#toc246) L'indépendance de l'expertise sanitaire en question

[^loi-post-mediator]: _"Car ce n’est pas la loi Bertrand, mais la loi post-Mediator, pour qu’il n’y ait plus de scandalede ce type."_ [Interview de Xavier Bertrand](http://www.liberation.fr/societe/2011/09/27/ce-n-est-pas-la-loi-bertrand-mais-la-loi-post-mediator_763882) au journal Libération le 27 septembre 2011, qui expose clairement le projet de loi.

## Une transparence limitée

Depuis 2014, la base Transparence-Santé est alimentée tous les 6 mois par les industriels. 
Ces données détaillées *pourraient* être "un puissant outil d'assainissement des pratiques des entreprises"[^cour-p65].

Malheureusement, la transparence escomptée est limitée dans les faits :

1. Le [moteur de recherche public](https://www.transparence.sante.gouv.fr/flow/interrogationAvancee?execution=e2s1) est **difficile d'emploi**

   Lister les déclarations concernant un médecin nécessite plusieurs étapes manuelles fastidieuses.
   
1. Le site ne propose **aucune vision statistique**

   Il est impossible d'avoir une vision d'ensemble des déclarations d'une entreprise[^cour-p65]. 
   
   Comme le résume le syndicat des entreprises pharmaceutiques, "De la pédagogie mais également des améliorations du site sont aujourd’hui indispensables pour permettre à la transparence d’atteindre pleinement ses objectifs."[^leem] 
   

1. Les déclarations sont de **mauvaise qualité** 
   
   Les déclarations transmises par les industriels sont hétérogènes et incomplètes.
   Il semble que les fichiers sont ingérés dans la base sans vérification technique ni harmonisation.
   
   Cette médiocrité des données brouille les pistes à tous les niveaux. 
   Les professionnels de santé sont souvent mal identifiés par leur n°RPPS[^rpps], et donc introuvables.
   

1. Les déclarations ne sont **pas contrôlées**, donc douteuses

   Les déclarations des entreprises ne sont pas certifiées par des organismes d'audit indépendants. 
   En cas de déclaration volontairement irrégulière, une entreprise encourt 45 000 € d'amende[^amende],
   une somme négligeable au vue des dépenses engagées. 


1. De nombreuses **informations sont tenues secrètes**

   Le décret d'application de la loi Bertrand avait largement vidé la loi de sa substance[^cnom-decret] :
   - les rémunérations ne sont pas rendues publiques
   - les bénéficiaires finaux ne sont pas indiqués
   - l'objet des contrats est tenu secret
   
  Suite à un recours de l'Ordre des Médecins, le Conseil d'État a en partie [annulé](https://www.legifrance.gouv.fr/affichJuriAdmin.do?idTexte=CETATEXT000030283091) ce décret. 
  Cette décision a été complétée par un [décret fin 2016](https://www.legifrance.gouv.fr/eli/decret/2016/12/28/AFSX1637582D/jo/texte) et une [ordonnance début 2017](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033893406&categorieLien=id).
  
  Dorénavant, les industriels doivent publier le montant des rémunérations, et identifier les bénéficiaires finaux des avantages.
  
  L'objet des contrats reste lui toujours protégé par la loi, ce qui renforce les suspicions de corruption au lieu de rétablir la confiance.

[^cour-p65]: Rapport de la Cour des Comptes de mars 2016, "La prévention des conflits d'intérêts en matière d'expertise sanitaire". [page 65-66](https://www.ccomptes.fr/sites/default/files/EzPublish/20160323-prevention-conflits-interets-en-matiere-expertise-sanitaire.pdf#page=65)

[^leem]: Citation du LEEM (syndicat de l'industrie pharmaceutique), en [réponse](http://www.vrai-faux.leem.org/post/167273340191/publi%C3%A9-dans-le-canard-encha%C3%AEn%C3%A9-le-8-novembre-2017) au [Canard Enchainé](https://www.slideshare.net/Market_iT/le-canard-enchain-20171108-des-labos-soignant-lthique) : _**Oui, la lisibilité de la base transparence mériterait d’être améliorée.**  La limite de la transparence aujourd’hui réside dans la complexité du dispositif mis en place, qui le rend difficilement lisible par le public. Des chiffres sont publiés sur la base transparence, mais les outils nécessaires pour les comprendre sont insuffisants voire inexistants. De la pédagogie mais également des améliorations du site sont aujourd’hui indispensables pour permettre à la transparence d’atteindre pleinement ses objectifs._  

[^rpps]: Le répertoire partagé des professionnels de santé ([RPPS](https://www.ameli.fr/medecin/exercice-liberal/vie-cabinet/installation-liberal/rpps)) est le fichier de référence des professionnels de santé. Le n° RPPS est un identifiant unique.

[^cnom-decret]: Article du CNOM du 23.05.2013, "Décret sur la publication des liens d’intérêt et la transparence : nous sommes très loin du compte" [url](https://www.conseil-national.medecin.fr/article/decret-sur-la-publication-des-liens-d%E2%80%99interet-et-la-transparence-nous-sommes-tres-loin-du-compte-1325)

[^amende]: Loi Bertrand, TITRE Ier : TRANSPARENCE DES LIENS D'INTÉRÊTS, Chapitre III : Sanctions pénales [url](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=7C5FE4786F2112680B0A4B6BD69AE001.tpdila09v_1?idArticle=JORFARTI000025053642&cidTexte=JORFTEXT000025053440&dateTexte=29990101)

## Des organes de contrôle impuissants 

Avant de signer certaines conventions, les industriels doivent consulter l'Ordre des Médecins. 
L'ordre émet alors un avis, qui n'a cependant **pas de pouvoir contraignant**[^ordre]. 

En lisant le rapport de la Cour des Comptes de mars 2016, on apprend par ailleurs que :[^cour-p62]
- les avis du CNOM concernant les conventions d'honoraires étaient massivement négatifs en 2015 (71%)    
- certains industriels refusent de soumettre des contrats à l'avis de l'ordre

La DGCCRF a indépendamment lancé une série d'enquêtes, qui n'ont quasiment jamais abouti.
Ces enquêtes ont notamment été entravées par des écrans placés entre les industriels et les bénéficiaires finaux[^cour-p32], selon trois stratégies : 

- des subventions sont versées à des associations de médecins
- les événements à destination des médecins sont organisés par des entreprises prestataires
- des filiales étrangères prennent en charge certaines dépenses lors des congrès

Cette [interview](https://www.macsf-exerciceprofessionnel.fr/Responsabilite/Humanisme-deontologie/conflits-interets-professionnels-de-sante-laboratoires) datant de 2016 du président du Conseil National de l’Ordre des Médecins est un bon résumé de la situation.

[^ordre]: Article du CNOM, du 04.10.2011, soit avant la promulgation de la loi Bertrand. "Conflits d’intérêts : pour restaurer la confiance l’Ordre préconise des mesures réglementaires et législatives" [url](https://www.conseil-national.medecin.fr/article/conflits-d%E2%80%99interets-pour-restaurer-la-confiance-l%E2%80%99ordre-preconise-des-mesures-reglementaires-et-legi-1114) 
    
[^cour-p62]: Rapport de la Cour des Comptes de mars 2016, "La prévention des conflits d'intérêts en matière d'expertise sanitaire". [page 62](https://www.ccomptes.fr/sites/default/files/EzPublish/20160323-prevention-conflits-interets-en-matiere-expertise-sanitaire.pdf#page=62)

[^cour-p32]: Rapport de la Cour des Comptes de mars 2016, "La prévention des conflits d'intérêts en matière d'expertise sanitaire". [page 32](https://www.ccomptes.fr/sites/default/files/EzPublish/20160323-prevention-conflits-interets-en-matiere-expertise-sanitaire.pdf#page=32)

