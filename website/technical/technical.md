---
permalink: /technical
title: Informations techniques
---
Profiling des fichiers sources (mai 2017):

- [Déclarations Rémunération]({{base}}/technical/declaration_remuneration)
- [Déclarations Avantage]({{base}}/technical/declaration_avantage)
- [Déclarations Conventions]({{base}}/technical/declaration_convention)
- [Annuaire Santé]({{base}}/technical/annuaire_sante)


[Traitement des données]({{base}}/technical/data)