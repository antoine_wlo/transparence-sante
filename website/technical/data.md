---
permalink: /technical/data
title: Traitement des données
---

## Données

### Fichiers

L'archive de données publiées sur le site [Data Gouv](https://www.data.gouv.fr/fr/datasets/transparence-sante-1/) comprend 4 fichiers csv:

- un annuaire des entreprises ayant effectué une déclaration
- 3 fichiers de déclarations: 
    - **conventions** qui liste les contrats
    - **rémunérations** versées en contrepartie d'une prestation
    - **avantages** qui liste les cadeaux offerts sans contrepartie 

L'[annuaire santé](https://annuaire.sante.fr/) (RPPS)est  également téléchargé. 
Il servira à obtenir des informations fiables sur les bénéficiaires physiques déclarées par les entreprises.


### Tables PostgreSQL 

Chacun de ces fichiers est nettoyé, puis ingéré dans une table de la base PostgreSQL, respectivement appelées :
- **entreprise** 
- **declaration_convention**
- **declaration_avantage** 
- **declaration_remuneration**
- **annuaire**

Une vue **declaration** reprend les colonnes communes des 3 tables de déclarations, pour simplifier des analyses communes.

### Vues matérialisées 

Des vues matérialisées stockent des résultats aggrégées, de façon à répondre immédiatements aux requêtes correspondantes.

Metabase ne permet pas de filtrer simplement plusieurs tables différentes. 
Les tableaux qui exposent ces tables ont donc un intérêt technique, mais peu d'intérêt analytique :

1. **declaration_sum_montant_ttc_by_entreprise_émmetrice** ⇒ [Top montant par entreprise](http://www.eurosfordocs.fr/metabase/dashboard/34)
1. **declaration_aggregate_sum_ttc_by_nom_prénom**  ⇒ [Total par bénéficiaire ](http://www.eurosfordocs.fr/metabase/dashboard/9)

Ces vues matérialisées seront utiles pour le développement de visualisations *ad hoc*, basées sur une API.


## Accélérer les analyses *ad hoc*


Pour servir des tableaux de bord interactifs, avec un temps de réponse inférieur à la seconde, trois solutions sont utilisées :
- Metabase sauvegarde en cache les résultats des analyses déjà effectuées.
- des `index`, qui fonctionnent de façon similaires aux index des livres. Un index sur les bénéficiaires permet ainsi de lire directement les déclarations qui le concernent.
- des `aggrégats` pré-calculés, enregistrés dans PostgreSQL. On enregistre ainsi pour chaque entreprise la somme des montants déclarés, plutôt que de reproduire une gigantesque addition à chaque requête.

Les analyses *ad hoc* ne peuvent pas utiliser ces optimisations. Elles prendront donc souvent plusieurs minutes. 

Des solutions techniques pour accélérer les requêtes personnalisées sur Metabase pourront être étudiées si besoin.
