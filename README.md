*This project aims to make it easy to use [Transparence Santé](www.transparence.sante.gouv.fr) dataset, 
as inspired by ProPublica's [DollarsForDocs](https://projects.propublica.org/docdollars) project.
It supports [EurosForDocs.fr](http://www.eurosfordocs.fr/) website*
 
Ce projet a pour objectif de simplifier l'utilisation de la base [Transparence-Santé](www.transparence.sante.gouv.fr), 
de façon similaire au site [DollarsForDocs](https://projects.propublica.org/docdollars) créé par ProPublica.
Il est en ligne à l'adresse [EurosForDocs.fr](http://www.eurosfordocs.fr/).

# Installation

Installation process should work on UNIX systems (Mac, Linux). 

Some command may not work on Windows (please help to fix them if you find errors).

## Clone project 

Execute the following commands in a terminal 

    git clone https://gitlab.com/eurosfordocs/transparence-sante.git
    cd transparence-sante
    cp .env-template .env

## Python data processing 

For local development, we recommend to install dependencies using `virtualenv`.

**Requirements**
- Python 3.5, 3.6 or 3.7
- pip (or pip3)
- virtualenv (`pip install virtualenv)

Note: 
- Installation fails with Python 3.4 because of pandas version
- Development has been done with Python 3.6
- Tests pass with Python 3.5 and 3.7  

**Installation** 

Create virtual environment and activate it.

    virtualenv venv --python=python3
    source venv/bin/activate

Install Python dependencies 

    pip3 install -r requirements.txt

**Test**

This should run >35 tests with success

    pytest tests

**Usage** 

If not already done, activate the virtualenv. 

    source venv/bin/activate
    
The script `main.py` is the entrypoint to run the Python Code, with the following usages :   

    ./main.py download  # Download and extract csv files to data/raw
    ./main.py clean     # Clean csv files to data/cleaned
    ./main.py upload    # Upload csv to PostgreSQL (cf Docker), create views and indexes 

## Running the whole project with Docker-Compose

This project uses **Docker** to run PostgreSQL, Metabase, and others services.

You can even run all Python data processing in a Docker image.   

![architecture](images/architecture.png)

**Requirements**
- [Docker]((https://docs.docker.com/engine/installation/))
- [Docker-Compose]((https://docs.docker.com/compose/install/))

**Installation**

*Note: this will download heavy Docker images, and may take some times to build them.* 

    docker-compose up           # <Ctrl> + C to stop
 
**Test**

When all services are up

- [localhost](http://localhost) serve website's homepage
- Metabase is running at [localhost/metabase/](http://localhost/metabase/)
- Run Python tests `docker-compose run --rm python pytest tests`

**Usage**

See Docker-Compose documentation. 

Run `./docker_compose_helper.py` to see some common commands.


# Contributing

Your contributions are most welcome ! :-)

Please look at 
- [Gitlab issues](https://gitlab.com/eurosfordocs/transparence-sante/issues)
- [Contributing page](http://www.eurosfordocs.fr/contributing/) on website

## Integrated Development Environment

We recommend to use an IDE such as PyCharm to navigate and modify the codebase.

**Pycharm Tips**
- The professional version comes with helpful Docker's and Databases' interfaces.
- Exclude the following directories from indexing `(right click > Mark directory as)` : 
venv, data, docker/*/data, notebooks/.ipynb_checkpoints


## Jupyter

Jupyter notebook are a great way to interactively explore and analyze data.

**Usage**  
    
    cd notebooks/ignore
    jupyter notebook

## JupyTEXT

Jupyter notebook have some drawbacks :
- They use a json format, that makes them impossible to properly manage versions with git
- They integrate badly with the rest of the codebase (ex: no refactoring of notebook code)

To collaborate on Jupyter Notebooks, we use [JupyTEXT](https://github.com/mwouts/jupytext) plugin. 

JupyTEXT allows to read and write Jupyter notebooks as paired `.py` files.

**Usage**

    cd notebooks
    jupyter notebook --config="config.py"
    # or 
    ./jupyter.sh
